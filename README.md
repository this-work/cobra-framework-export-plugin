# Cobra-Framework Export Plugin
Nuxt module for static SCORM exports. Requires a minimal version of nuxt 2.17+


### Requirements
- Nuxt 2.17+


### Usage in Nuxt

Install plugin dependencies
``` bash
$ npm install @this/cobra-framework-export-plugin
```

Add module in nuxt.config.js
``` js
modules: [
    ['@this/cobra-framework-export-plugin'],
]
```

